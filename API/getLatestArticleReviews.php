<?php

include 'db_conn.php';

$sql = "SELECT * FROM journal_review 
WHERE (year = (SELECT MAX(year) FROM journal WHERE published = 1)
AND num = (SELECT MAX(num) FROM journal WHERE (published = 1 AND year = (SELECT MAX(year) FROM journal WHERE published = 1) )))"; 

// run SQL statement
$result = mysqli_query($con,$sql);

// die if SQL statement failed
if (!$result) {
  http_response_code(404);
  die(mysqli_error($con));
}

if (!$id) echo '[';
    for ($i=0 ; $i<mysqli_num_rows($result) ; $i++) {
      echo ($i>0?',':'').json_encode(mysqli_fetch_object($result));
    }
    if (!$id) echo ']';

/* if ($method == 'GET') {
    if (!$id) echo '[';
    for ($i=0 ; $i<mysqli_num_rows($result) ; $i++) {
      echo ($i>0?',':'').json_encode(mysqli_fetch_object($result));
    }
    if (!$id) echo ']';
  } elseif ($method == 'POST') {
    echo json_encode($result);
  } else {
    echo mysqli_affected_rows($con);
  } */

$con->close();
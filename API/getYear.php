<?php

include 'db_conn.php';

//$sql = "SELECT year, vol, num FROM journal GROUP BY year, num ORDER BY year DESC"; 


$sql = "SELECT * FROM journal LEFT JOIN journal_time ON journal.num = journal_time.id  WHERE (published = 1) GROUP BY year, num ORDER BY year DESC, num DESC"; 

// run SQL statement
$result = mysqli_query($con,$sql);

// die if SQL statement failed
if (!$result) {
  http_response_code(404);
  die(mysqli_error($con));
}

if (!$id) echo '[';
    for ($i=0 ; $i<mysqli_num_rows($result) ; $i++) {
      echo ($i>0?',':'').json_encode(mysqli_fetch_object($result));
    }
    if (!$id) echo ']';

/* if ($method == 'GET') {
    if (!$id) echo '[';
    for ($i=0 ; $i<mysqli_num_rows($result) ; $i++) {
      echo ($i>0?',':'').json_encode(mysqli_fetch_object($result));
    }
    if (!$id) echo ']';
  } elseif ($method == 'POST') {
    echo json_encode($result);
  } else {
    echo mysqli_affected_rows($con);
  } */

$con->close();
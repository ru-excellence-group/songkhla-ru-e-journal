import React from 'react';
import './Contact.css';

function Contact () {
  return (
    <div className="App">
      <div className="mFix">
        <div className="addM">
          <div className="maxWidth textCenter boardList">

            <div>
              <h2>เจ้าของวารสาร</h2> {/* ---Updated 17/05/2019--- */}
              <h4>
                มหาวิทยาลัยรามคำแหง สาขาวิทยบริการเฉลิมพระเกียรติ จังหวัดสงขลา
                <br />
                ที่ตั้ง หมู่ที่ 6 ถ.ควนจีน ต.ควนลัง อ.หาดใหญ่ จ.สงขลา 90110
              </h4>
              <h5>คณะบริหารธุรกิจ มหาวิทยาลัยรามคำแหง</h5>
              <br />
              <span>
                <a
                  href="http://www.songkhla.ru.ac.th"
                  rel="noopener noreferrer"
                >
                  <h5>Website: www.songkhla.ru.ac.th</h5>
                </a>
                {/* <h5>excmba@hotmail.com</h5> */}
              </span>
              {/* <span>
                 <a
                  href="tel:023108213"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <h5>โทร 02-310-8213</h5>
                </a> 
              </span>*/}
            </div>

          </div>
        </div>
      </div>
    </div>
  );
}

export default Contact;

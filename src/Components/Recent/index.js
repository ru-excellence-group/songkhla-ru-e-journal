import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './Recent.css';

const apiUrl = 'http://www.songkhla.ru.ac.th/e-journal/api/';

export default class Recent extends Component {
  state = {
    articles_set: [
      {
        year: '',
        vol: '',
        num: '',
        time_title: '',
        time_title_en: '',
      },
    ],
    initials: false,
    stack: null,
  };
  componentDidMount () {
    fetch (apiUrl + 'getYear.php')
      .then (response => {
        return response.json ();
      })
      .then (data => {
        this.setState ({
          articles_set: data
            .filter (articles_set => !!articles_set.vol)
            .map (articles_set => ({
              year: articles_set.year,
              vol: articles_set.vol,
              num: articles_set.num,
              time_title: articles_set.time_title,
              time_title_en: articles_set.time_title_en,
            })),
        });
        // console.log (this.state.articles_set);
        console.log ('Fetch Recent Articles Year Published Success!');
      });

    this.setState ({initials: true});
  }
  render () {
    if (this.state.articles_set.length >= 1 && this.state.initials === true) {
      var articleCount = null;
      var stack = null;
      return (
        <div className="recent addM">
          <div className="maxWidth">
            <h2 className="sectionHeader">
              ฉบับย้อนหลัง / Recent Issues
            </h2>
            <div className="cardBlock">
              <ul className="columnList">
                {this.state.articles_set.map ((article, index0) => {
                  articleCount = article.vol;
                  if (index0 !== 0 && stack !== articleCount) {
                    //this.setState ({stack: articleCount});
                    stack = articleCount;
                    const year_ = Number (article.year) - 543;
                    return (
                      <li className="columnListTitle" key={index0}>
                        <div>
                          <h3>
                            ปีที่
                            {' '}
                            {article.vol}
                            {' '}
                            (
                            {article.year}
                            ) Vol.
                            {' '}
                            {article.vol}
                            {' '}
                            ({year_})
                          </h3>
                        </div>

                        {this.state.articles_set.map ((articleList, index1) => {
                          const url =
                            process.env.PUBLIC_URL +
                            '/p/' +
                            articleList.vol +
                            '/' +
                            articleList.num;
                          if (
                            articleList.vol === articleCount &&
                            index1 !== 0
                          ) {
                            return (
                              <ul className="columnSubList" key={index1}>
                                <li>
                                  <Link to={url}>
                                    {' '}
                                    ฉบับที่
                                    {' '}
                                    {articleList.num}
                                    {' '}
                                    {articleList.time_title}
                                    {' '}
                                    / No.
                                    {' '}
                                    {articleList.num}
                                    {' '}
                                    {articleList.time_title}
                                  </Link>
                                </li>
                              </ul>
                            );
                          } else {
                            return null;
                          }
                        })}

                      </li>
                    );
                  } else {
                    return null;
                  }
                })}
              </ul>
            </div>
          </div>
        </div>
      );
    } else return null;
  }
}

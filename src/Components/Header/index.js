import React, {Component} from 'react';
import {Link, NavLink} from 'react-router-dom';
import './Header.css';

export default class Header extends Component {
  render () {
    return (
      <div className="header">
        <ul id="header-primary" className="fixHeaderLongText">
          <li><Link to={`${process.env.PUBLIC_URL}/`}>E-JOURNAL</Link></li>
        </ul>
        <ul id="header-secondary">
          <li className="hamburgurWrapper">
            <div><div className="hamburgur" /></div>
          </li>
          <li>
            <NavLink
              exact={true}
              activeClassName="--active"
              to={`${process.env.PUBLIC_URL}/`}
            >
              หน้าหลัก
            </NavLink>
          </li>
          <li>
            <NavLink
              activeClassName="--active"
              to={`${process.env.PUBLIC_URL}/board`}
            >
              กองบรรณาธิการ
            </NavLink>
          </li>
          {/* <li>
            <NavLink
              activeClassName="--active"
              to={`${process.env.PUBLIC_URL}/writer`}
            >
              คำแนะนำสำหรับผู้เขียน
            </NavLink>
          </li> */}
          <li>
            <NavLink
              activeClassName="--active"
              to={`${process.env.PUBLIC_URL}/contact`}
            >
              ติดต่อสอบถาม
            </NavLink>
          </li>
        </ul>
      </div>
    );
  }
}

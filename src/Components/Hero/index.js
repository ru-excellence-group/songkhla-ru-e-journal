import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './Hero.css';

export default class Hero extends Component {
  render () {
    return (
      <div className="hero">
        <div>
          <Link to={`${process.env.PUBLIC_URL}/`}>
            <h2 id="heroHeader">
              <span>วารสาร</span>วิจัยธุรกิจและการจัดการ เพื่อความเป็นเลิศ
            </h2>
            <h3>
              <span>Journal of</span>
              {' '}
              Business and Management Research for Excellence
            </h3>
          </Link>
          <p>
            มหาวิทยาลัยรามคำแหง สาขาวิทยบริการเฉลิมพระเกียรติ จังหวัดสงขลา
          </p>
        </div>
      </div>
    );
  }
}

import React, {Component} from 'react';
//import {Link, NavLink} from 'react-router-dom';
import './Footer.css';

export default class Footer extends Component {
  render () {
    return (
      <div className="footer">
        <div>
          <h5 id="footerTitle">
            Copyright © 2019
            RAMKHAMHAENG UNIVERSITY
            SONGKHLA CAMPUS IN HONOUR MAJESTY THE KING, SONGKLHA.
            Tel.074-251-333.
            <br />
            Design by Suparerk Yoscharoen
          </h5>
        </div>
      </div>
    );
  }
}

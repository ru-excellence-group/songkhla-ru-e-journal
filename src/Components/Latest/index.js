import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './Latest.css';

const apiUrl = 'http://www.songkhla.ru.ac.th/e-journal/api/';

export default class Latest extends Component {
  state = {
    value: '',
    articles: [
      {
        id: '',
        vol: '',
        year: '',
        num: '',
        author: '',
        title: '',
        file_url: '',
        img_url: '',
      },
    ],
    reviews: [
      {
        review_id: '',
        review_title: '',
        review_url: '',
      },
    ],
    editorials: [
      {
        editorials_id: '',
        editorials_year: '',
        editorials_num: '',
        editorial_url: '',
      },
    ],
    heroText: [
      {
        en: '',
        th: '',
      },
    ],
    initials: false,
    error: false,
  };

  componentDidMount () {
    fetch (apiUrl + 'getTheme.php?type=hero')
      .then (response => {
        return response.json ();
      })
      .then (data => {
        this.setState ({
          heroText: data.map (heroText => ({
            en: heroText.text_en,
            th: heroText.text_th,
          })),
        });
        //console.log (this.state.heroText);
        console.log ('Fetch Theme Success!');
      })
      .catch (error => {
        console.log (error);
        this.setState ({error: true});
      });

    fetch (apiUrl + 'getLatestArticle.php')
      .then (response => {
        return response.json ();
      })
      .then (data => {
        this.setState ({
          articles: data
            .filter (articles => !!articles.title)
            .map (articles => ({
              id: articles.id,
              vol: articles.vol,
              year: articles.year,
              num: articles.num,
              author: articles.author,
              title: articles.title,
              file_url: articles.file_url,
              img_url: articles.img_url,
            })),
        });
        // console.log (this.state.articles);
        console.log ('Fetch Latest Article Success!');
      })
      .catch (error => {
        console.log (error);
        this.setState ({error: true});
      });

    fetch (apiUrl + 'getLatestArticleReviews.php')
      .then (response => {
        return response.json ();
      })
      .then (data => {
        this.setState ({
          reviews: data.filter (reviews => !!reviews.title).map (review => ({
            review_id: review.id,
            review_title: review.title,
            review_url: review.url,
          })),
        });
        // console.log (this.state.reviews);
        console.log ('Fetch Latest Article Book Review Success!');
      })
      .catch (error => {
        console.log (error);
        this.setState ({error: true});
      });

    fetch (apiUrl + 'getLatestEditorials.php')
      .then (response => {
        return response.json ();
      })
      .then (data => {
        this.setState ({
          editorials: data
            .filter (editorials => !!editorials.id)
            .map (editorial => ({
              editorial_id: editorial.id,
              editorial_year: editorial.year,
              editorial_num: editorial.num,
              editorial_url: editorial.url_file,
            })),
        });
        // console.log (this.state.reviews);
        console.log ('Fetch Latest Article Book Editorials Success!');
      })
      .catch (error => {
        console.log (error);
        this.setState ({error: true});
      });

    this.setState ({initials: true});
  }

  render () {
    //    console.log (this.state.articles.length);
    //  console.log ('test');
    let ReviewBox = (
      <div className="cardBlock">
        <h2>บทวิจารณ์หนังสือ / Book Reviews</h2>
        <ul className="columnList">
          <li className="columnListTitle">
            <ul className="columnSubList">
              {this.state.reviews.map (review => (
                <li key={review.review_id}>
                  <a href={review.review_url}>
                    {review.review_title}
                  </a>
                </li>
              ))}
            </ul>
          </li>
        </ul>
      </div>
    );
    if (
      this.state.reviews.length >= 1 &&
      this.state.articles.length >= 1 &&
      this.state.initials === true &&
      this.state.articles[0].id !== ''
    ) {
      ReviewBox = (
        <div className="cardBlock">
          <h2>บทวิจารณ์หนังสือ / Book Reviews</h2>
          <ul className="columnList">
            <li className="columnListTitle">
              <ul className="columnSubList">
                {this.state.reviews.map (review => (
                  <li key={review.review_id}>
                    <a
                      href={review.review_url}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {review.review_title}
                    </a>
                  </li>
                ))}
              </ul>
            </li>
          </ul>
        </div>
      );
    } else {
      ReviewBox = null;
    }

    if (
      this.state.articles.length >= 1 &&
      this.state.initials === true &&
      this.state.articles[0].id !== ''
    ) {
      const imgTest = this.state.articles[0].img_url;
      const vol = this.state.articles[0].vol;
      const year = this.state.articles[0].year;
      const year_num = Number (year);
      const year_en = year_num - 543;
      const num = this.state.articles[0].num;
      const urlx = process.env.PUBLIC_URL + '/p/' + vol + '/' + num;
      console.log (this.state.articles);
      return (
        <div className="latest addM">
          <div className="maxWidth">
            <h2 className="sectionHeader" id="firstHeaderCurrentIssue">
              ฉบับปัจจุบัน / Current Issue
            </h2>
            <div className="cardRowList">
              <Link to={urlx} className="card">
                <img alt="Cover" src={imgTest} />
                <div className="cardContent">
                  <h3>
                    ปีที่
                    {' '}
                    {vol}
                    {' '}
                    ฉบับที่
                    {' '}
                    {num}
                    {' '}
                    {year}
                    {' '}
                    <br />
                    {' '}
                    Vol.
                    {' '}
                    {vol}
                    {' '}
                    No.
                    {' '}
                    {num}
                    {' '}
                    {year_en}
                  </h3>
                </div>
              </Link>
              <div className="endCardCaption">
                <div className="cardContent">
                  <h3 id="heroText_th">
                    {this.state.heroText[0].th}
                  </h3>

                  <h3 id="heroText_en">
                    {this.state.heroText[0].en}
                  </h3>
                </div>
              </div>
            </div>

            <div className="cardBlock">
              <h2>บทบรรณาธิการ / Editorial</h2>
              <ul className="columnList">
                <li className="columnListTitle">
                  <ul className="columnSubList">
                    {this.state.editorials.map ((editorial, index) => (
                      <li key={index}>
                        <a
                          href={editorial.editorial_url}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <span className="textBold">บทบรรณาธิการ</span>
                        </a>
                      </li>
                    ))}
                  </ul>
                </li>
              </ul>
            </div>

            <div className="cardBlock">
              <h2>บทความ / Articles</h2>
              <ul className="columnList">
                <li className="columnListTitle">
                  <ul className="columnSubList">
                    {this.state.articles.map (article => (
                      <li key={article.id}>
                        <a
                          href={article.file_url}
                          target="_blank"
                          rel="noopener noreferrer"
                          onClick={() => {}}
                        >
                          <span className="textBold">{article.title}</span>
                          <br />
                          <span className="textTungsten">{article.author}</span>
                        </a>
                      </li>
                    ))}
                  </ul>
                </li>
              </ul>
            </div>

            {ReviewBox}

          </div>
        </div>
      );
    } else if (this.state.error === true) {
      return (
        <div className="noJournalFound latest addM">
          <div className="maxWidth">
            <h2
              className="sectionHeader textCenter"
              id="firstHeaderCurrentIssue"
            >
              ระบบขัดข้อง กรุณาติดต่อผู้ดูแล
            </h2>
          </div>
        </div>
      );
    } else if (
      this.state.articles.length === 0 &&
      this.state.initials === true
    ) {
      return (
        <div className="noJournalFound latest addM">
          <div className="maxWidth">
            <h2
              className="sectionHeader textCenter"
              id="firstHeaderCurrentIssue"
            >
              ขณะนี้ยังไม่มีบทความที่ได้รับการเผยแพร่
            </h2>
          </div>
        </div>
      );
    } else {
      return (
        <div className="noJournalFound latest addM">
          <div className="maxWidth">
            <h2
              className="sectionHeader textCenter"
              id="firstHeaderCurrentIssue"
            >
              Loading...
            </h2>
          </div>
        </div>
      );
    }
  }
}

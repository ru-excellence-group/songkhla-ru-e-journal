import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './404.css';

export default class NotFound extends Component {
  render () {
    return (
      <div id="notFound">
        <div>
          <h2>404.</h2>
          <p>The requested URL was not found.</p>
          <br />
          <Link className="button" to={`${process.env.PUBLIC_URL}/`}>
            กลับสู่หน้าหลัก
          </Link>
        </div>
      </div>
    );
  }
}

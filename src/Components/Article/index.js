import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './Article.css';

const apiUrl = 'http://www.songkhla.ru.ac.th/e-journal/api/';

export default class Article extends Component {
  state = {
    articles: [
      {
        id: '',
        vol: '',
        year: '',
        num: '',
        author: '',
        title: '',
        file_url: '',
        img_url: '',
      },
    ],
    reviews: [
      {
        review_id: '',
        review_title: '',
        review_url: '',
      },
    ],
    editorials: [
      {
        editorials_id: '',
        editorials_year: '',
        editorials_num: '',
        editorial_url: '',
      },
    ],
    current_vol: this.props.match.params.vol,
    current_num: this.props.match.params.num,
    heroText: [
      {
        en: '',
        th: '',
      },
    ],
    initials: false,
    error: false,
  };
  componentDidMount () {
    fetch (apiUrl + 'getTheme.php?type=hero')
      .then (response => {
        return response.json ();
      })
      .then (data => {
        this.setState ({
          heroText: data.map (heroText => ({
            en: heroText.text_en,
            th: heroText.text_th,
          })),
        });
        //console.log (this.state.heroText);
        console.log ('Fetch Theme Success!');
      })
      .catch (error => console.log (error));

    let current_vol = this.state.current_vol;
    let current_num = this.state.current_num;
    fetch (
      apiUrl + 'getSingleArticle.php?vol=' + current_vol + '&num=' + current_num
    )
      .then (response => {
        return response.json ();
      })
      .then (data => {
        this.setState ({
          articles: data
            .filter (articles => !!articles.title)
            .map (articles => ({
              id: articles.id,
              vol: articles.vol,
              year: articles.year,
              num: articles.num,
              author: articles.author,
              title: articles.title,
              file_url: articles.file_url,
              img_url: articles.img_url,
            })),
        });
        //console.log (this.state.articles);
        console.log ('Fetch Article Success!');
      })
      .catch (error => {
        console.log (error);
        this.setState ({error: true});
      });

    fetch (
      apiUrl +
        'getSingleArticleReviews.php?vol=' +
        current_vol +
        '&num=' +
        current_num
    )
      .then (response => {
        return response.json ();
      })
      .then (data => {
        this.setState ({
          reviews: data.filter (reviews => !!reviews.title).map (reviews => ({
            review_id: reviews.id,
            review_title: reviews.title,
            review_url: reviews.url,
          })),
        });
        //console.log (this.state.reviews);
        console.log ('Fetch Book Review Success!');
      })
      .then (data => {
        if (this.state.reviews.length === 0) {
          console.log ('No Book Review!');
        }
      })
      .catch (error => {
        console.log (error);
        this.setState ({error: true});
      });

    fetch (
      apiUrl +
        'getSingleEditorials.php?vol=' +
        current_vol +
        '&num=' +
        current_num
    )
      .then (response => {
        return response.json ();
      })
      .then (data => {
        this.setState ({
          editorials: data
            .filter (editorials => !!editorials.id)
            .map (editorial => ({
              editorial_id: editorial.id,
              editorial_year: editorial.year,
              editorial_num: editorial.num,
              editorial_url: editorial.url_file,
            })),
        });
        // console.log (this.state.reviews);
        console.log ('Fetch Latest Article Book Editorials Success!');
      })
      .catch (error => {
        console.log (error);
        this.setState ({error: true});
      });

    this.setState ({initials: true});
  }

  render () {
    let ReviewBox = <div className="cardBlock" />;
    if (
      this.state.reviews.length >= 1 &&
      this.state.articles.length >= 1 &&
      this.state.initials === true &&
      this.state.articles[0].id !== ''
    ) {
      ReviewBox = (
        <div className="cardBlock">
          <h2>บทวิจารณ์หนังสือ / Book Reviews</h2>
          <ul className="columnList">
            <li className="columnListTitle">
              <ul className="columnSubList">
                {this.state.reviews.map (review => (
                  <li key={review.review_id}>
                    <a
                      href={review.review_url}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <span className="textBold">{review.review_title}</span>
                    </a>
                  </li>
                ))}
              </ul>
            </li>
          </ul>
        </div>
      );
    } else {
      ReviewBox = null;
    }
    if (
      this.state.articles.length >= 1 &&
      this.state.initials === true &&
      this.state.articles[0].id !== ''
    ) {
      const imgTest = this.state.articles[0].img_url;
      const vol = this.state.articles[0].vol;
      const year = this.state.articles[0].year;
      const year_num = Number (year);
      const year_en = year_num - 543;
      const num = this.state.articles[0].num;
      const urlx = process.env.PUBLIC_URL + '/p/' + vol + '/' + num;
      return (
        <div className="mFix">
          <div className="latest addM">
            <div className="maxWidth">
              <h2 className="sectionHeader" id="firstHeaderCurrentIssue">
                <Link to={`${process.env.PUBLIC_URL}/`}>หน้าหลัก ></Link>
                {' '}
                ปีที่
                {' '}
                {vol}
                {' '}
                ฉบับที่
                {' '}
                {num}
                {' '}
                ({year})
                / Vol.
                {' '}
                {vol}
                {' '}
                No.
                {' '}
                {num}
                {' '}
                ({year_en})
              </h2>
              <div className="cardRowList" id="fixWidthHighCard">
                <Link to={urlx} className="card">
                  <img alt="Cover" src={imgTest} />
                  <div className="cardContent">
                    <h3>
                      ปีที่
                      {' '}
                      {vol}
                      {' '}
                      ฉบับที่
                      {' '}
                      {num}
                      {' '}
                      (
                      {year}
                      )
                      {' '}
                      <br />
                      {' '}
                      Vol.
                      {' '}
                      {vol}
                      {' '}
                      No.
                      {' '}
                      {num}
                      {' '}
                      (
                      {year_en}
                      )
                    </h3>
                  </div>
                </Link>
                <div className="endCardCaption">
                  <div className="cardContent">
                    <h3 id="heroText_th">
                      {this.state.heroText[0].th}
                    </h3>

                    <h3 id="heroText_en">
                      {this.state.heroText[0].en}
                    </h3>
                  </div>
                </div>
              </div>

              <div className="cardBlock">
                <h2>บทบรรณาธิการ / Editorial</h2>
                <ul className="columnList">
                  <li className="columnListTitle">
                    <ul className="columnSubList">
                      {this.state.editorials.map ((editorial, index) => (
                        <li key={index}>
                          <a
                            href={editorial.editorial_url}
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            <span className="textBold">บทบรรณาธิการ</span>
                          </a>
                        </li>
                      ))}
                    </ul>
                  </li>
                </ul>
              </div>

              <div className="cardBlock">
                <h2>บทความ / Articles</h2>
                <ul className="columnList">
                  <li className="columnListTitle">
                    <ul className="columnSubList">
                      {this.state.articles.map (article => (
                        <li key={article.id}>
                          <a
                            href={article.file_url}
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            <span className="textBold">{article.title}</span>
                            <br />
                            <span className="textTungsten">
                              {article.author}
                            </span>
                          </a>
                        </li>
                      ))}
                    </ul>
                  </li>
                </ul>
              </div>

              {ReviewBox}

            </div>
          </div>
        </div>
      );
    } else if (
      this.state.articles.length === 0 &&
      this.state.initials === true
    ) {
      return (
        <div className="noJournalFound latest addM mFix">
          <div className="maxWidth textCenter">
            <h2
              className="sectionHeader textCenter"
              id="firstHeaderCurrentIssue"
            >
              วรสาร
              {' '}
              ปีที่
              {' '}
              {this.state.current_vol}
              {' '}
              ฉบับที่
              {' '}
              {this.state.current_num}
              {' '}
              ยังไม่ได้รับการเผยแพร่
              <br />Journal Vol.
              {' '}
              {this.state.current_vol}
              {' '}
              No.
              {' '}
              {this.state.current_num}
              {' '}
              isn't published yet
              {/* ขณะนี้ยังไม่มีบทความที่ได้รับการเผยแพร่ */}
            </h2>
            <Link className="button" to={`${process.env.PUBLIC_URL}/`}>
              กลับสู่หน้าหลัก
            </Link>
          </div>
        </div>
      );
    } else {
      return (
        <div className="noJournalFound latest addM">
          <div className="maxWidth">
            <h2
              className="sectionHeader textCenter"
              id="firstHeaderCurrentIssue"
            >
              Loading...
            </h2>
          </div>
        </div>
      );
    }
  }
}

import React from 'react';
import ReactDOM from 'react-dom';
import {Route, Switch, Router} from 'react-router-dom';
import ReactGA from 'react-ga';
import {createBrowserHistory} from 'history';
import './index.css';

import Header from './Components/Header';
import Hero from './Components/Hero';

import App from './App';
import Article from './Components/Article';
import Board from './Components/Board';
import Contact from './Components/Contact';

import NotFound from './Components/404';

import * as serviceWorker from './serviceWorker';
import Footer from './Components/Footer';

const history = createBrowserHistory ();
ReactGA.initialize ('UA-130555372-5');
history.listen ((location, action) => {
  ReactGA.pageview (location.pathname + location.search);
  console.log (location.pathname);
});

const routing = (
  <Router history={history}>
    <div id="wrapper">
      <Header />
      <Hero />
      <Switch>
        <Route exact path={`${process.env.PUBLIC_URL}/`} component={App} />
        <Route
          exact
          path={`${process.env.PUBLIC_URL}/board`}
          component={Board}
        />
        <Route
          exact
          path={`${process.env.PUBLIC_URL}/contact`}
          component={Contact}
        />
        <Route
          exact
          path={`${process.env.PUBLIC_URL}/p/:vol/:num`}
          component={Article}
        />
        <Route component={NotFound} />
      </Switch>
      <Footer />
    </div>
  </Router>
);

ReactDOM.render (routing, document.getElementById ('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register ();

import React from 'react';
import './App.css';
import Latest from './Components/Latest';
import Recent from './Components/Recent';

function App () {
  return (
    <div className="App">
      <div className="mFix">
        <Latest />
        <Recent />
      </div>
    </div>
  );
}

export default App;

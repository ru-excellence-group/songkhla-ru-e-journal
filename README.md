## Songkhla RU E-Journal

Ramkhamheang University Songkhla Campus<br>
Design & Build by Suparerk in the name of blazelab

This project using the following technologies:

- [React](https://facebook.github.io/react/) and [React Router](https://reacttraining.com/react-router/) for the frontend
- [phpMyAdmin](https://www.phpmyadmin.net/)
- [Sass](https://sass-lang.com/) for styles (using the SCSS syntax)

## Requirements

- [Node.js](https://nodejs.org/en/)
- `npm install`

## Running

### `npm start`
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.